// +build phyz

package pkg

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestListDeviceTypes(t *testing.T) {
	dt, err := ListDeviceTypes()

	assert.NoError(t, err)
	assert.NotNil(t, dt)
}

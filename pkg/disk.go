package pkg

import (
	"fmt"
	"io"
	"os"
)

const (
	mbrSectorSize              = 512
	mbrSignOffset              = 510
	mbrSignByteA          byte = 0x55
	mbrSignByteB          byte = 0xAA
	mbrPartitionsOffset        = 446
	mbrPartitionBlockSize      = 16

	BootSectorTypeMBR = BootSectorType("MBR")
	BootSectorTypeGPT = BootSectorType("GPT")

	PartitionTypeLVM   = PartitionType("LVM")
	PartitionTypeEmpty = PartitionType("EMPTY")

	EmptyPartitionByte     byte = 0
	LVMPartitionByte       byte = 0x8E
	HybridGPTPartitionByte byte = 0xED
	GPTPartitionByte       byte = 0xEE
)

type (
	DiskStorage struct {
		Disks []*Disk
	}

	BootSectorType string
	PartitionType  string

	Disk struct {
		Path       string
		BootSector BootSectorType
		Partitions []Partition
	}

	Partition struct {
		Name       string
		Type       PartitionType
		MountPoint string
	}

	DeviceType struct {
		Code byte
		Name string
	}
)

var (
	ErrNonMBR = fmt.Errorf("MBR sign is invalid")
)

func readMBR(path string) (*Disk, error) {
	r, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	defer func() {
		if err := r.Close(); err != nil {
			fmt.Printf("[ERROR] can't close descriptor for path %s", path)
		}
	}()

	b := make([]byte, mbrSectorSize)
	if _, err := r.Read(b); err != nil {
		return nil, err
	}

	if b[mbrSignOffset] != mbrSignByteA || b[mbrSignOffset+1] != mbrSignByteB {
		return nil, ErrNonMBR
	}

	parts, err := readMBRPartitions(r)
	if err != nil {
		return nil, err
	}

	return &Disk{Path: path, Partitions: parts}, nil
}

func readMBRPartitions(r io.ReadSeeker) ([]Partition, error) {
	parts := make([]Partition, 0)

	for i := 1; i <= 4; i++ {
		offsetValue := int64(mbrPartitionsOffset + mbrPartitionBlockSize*(i-1))
		if _, err := r.Seek(offsetValue, 0); err != nil {
			return nil, err
		}

		pb := make([]byte, mbrPartitionBlockSize)
		if _, err := r.Read(pb); err != nil {
			return nil, err
		}

	}

	return parts, nil
}

package pkg

import (
	"time"
)

type Config struct {
	Threads        int
	TestDuration   time.Duration
	IOTestDuration time.Duration
	DiskAllocation float64
	Device         string
	Path           string
	RushMode       bool
	Verbose        bool
}

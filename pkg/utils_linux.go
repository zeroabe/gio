package pkg

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"

	"golang.org/x/sys/unix"
)

const (
	linuxMounts      = "/proc/self/mounts"
	entriesSeparator = " "
)

func (u *util) GetDiskMountPoint(device string) (string, error) {
	mnt, err := os.Open(linuxMounts)
	if err != nil {
		return "", err
	}

	reader := bufio.NewReader(mnt)
	for {
		line, isPref, err := reader.ReadLine()
		if err != nil {
			if err == io.EOF {
				return "", fmt.Errorf("device %s isn't mounted", device)
			}

			return "", err
		}

		if isPref {
			// TODO handle long lines
			continue
		}

		entries := strings.Split(string(line), entriesSeparator)
		if len(entries) < 2 {
			continue
		}

		if entries[0] == device {
			return entries[1], nil
		}
	}
}

func (u *util) GetPhysicalMemorySize() uint64 {
	info := unix.Sysinfo_t{}
	if err := unix.Sysinfo(&info); err != nil {
		return 0
	}

	return info.Totalram * uint64(info.Unit)
}

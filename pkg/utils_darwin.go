package pkg

import (
	"golang.org/x/sys/unix"
)

// #include <unistd.h>
import "C"

func (u *util) GetDiskMountPoint(device string) (string, error) {
	fs := unix.Statfs_t{}
	if err := unix.Statfs(device, &fs); err != nil {
		return "", err
	}

	path := unix.ByteSliceToString(fs.Mntonname[:])

	return path, nil
}

func (u *util) GetPhysicalMemorySize() uint64 {
	return uint64(C.sysconf(C._SC_PHYS_PAGES) * C.sysconf(C._SC_PAGE_SIZE))
}

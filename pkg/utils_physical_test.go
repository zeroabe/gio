// +build phyz

package pkg

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	u      *util
	device = "/dev/sda"
)

func TestMain(m *testing.M) {
	args := os.Args

	if len(args) > 0 {
		device = args[1]
	}
}

func TestGetDiskMountPoint(t *testing.T) {
	mp, err := u.GetDiskMountPoint(device)

	assert.NoError(t, err)

	t.Logf("Disk mounted on %s\n", mp)
}

func TestGetDiskBlockSize(t *testing.T) {
	b, err := u.GetDiskBlockSize(device)
	assert.NoError(t, err)

	t.Logf("Block size is %d\n", b)
}

func TestIsLVM(t *testing.T) {
	is, err := u.IsLVM(device)

	assert.NoError(t, err)
	t.Logf("device lvm: %v", is)
}

func TestIsEmpty(t *testing.T) {
	e, err := u.IsEmpty(device)

	assert.NoError(t, err)
	t.Logf("Disk emptiness is %v", e)
}

package pkg

import (
	"context"
	"fmt"
)

func (t *Runner) Execute() error {
	if err := t.selfCheck(); err != nil {
		return err
	}

	maxBlockSize, err := t.util.GetDiskBlockSize(t.filename)
	if err != nil {
		return err
	}

	if maxBlockSize <= MinBlockSize {
		maxBlockSize = MaxBlockSize
	}

	fmt.Printf("Starting... Seconds to run: %0.2f, threads to use: %d, disk space to use (MiB): %d\n",
		t.Config.TestDuration.Seconds(), t.Config.Threads, int(t.Config.DiskAllocation*(1<<10)))

	if !t.Config.RushMode {
		t.generateBlock(MinBlockSize)
		lowCtx, lowCancel := context.WithTimeout(context.Background(), t.Config.TestDuration)
		defer func() {
			lowCancel()
			t.cleanUp()
		}()

		if err := t.runWithBlockSize(lowCtx, MinBlockSize, &t.LowIO); err != nil {
			return fmt.Errorf("can't perform low IO benchmark: %v", err)
		}
	}

	t.generateBlock(maxBlockSize)
	bwCtx, bwCancel := context.WithTimeout(context.Background(), t.Config.TestDuration)

	defer bwCancel()

	if err := t.runWithBlockSize(bwCtx, maxBlockSize, &t.Bandwidth); err != nil {
		return fmt.Errorf("can't perform IO bandwidth benchmark: %v", err)
	}

	return nil
}

func (t *Runner) runWithBlockSize(ctx context.Context, blockSize int, result *Result) error {
	stop := make(chan struct{})
	errChan := make(chan error)

	defer close(stop)
	defer close(errChan)

	go func(ctx context.Context, stop chan<- struct{}, errChan chan<- error) {
		result = NewResult(blockSize)
		if err := t.runIOPSTest(ctx, blockSize, result); err != nil {
			errChan <- err
			return
		}

		fmt.Printf("%s", result)

		stop <- struct{}{}
	}(ctx, stop, errChan)

	for {
		select {
		case err := <-errChan:
			return err
		case <-stop:
			return nil
		}
	}
}

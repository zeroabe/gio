package pkg

import (
	"fmt"
	"os"
	"path"
	"time"
)

const (
	CreateFileBlockSize = 1 << 20 // 1 MiB
)

type Runner struct {
	util      Utility
	Start     time.Time
	Config    *Config
	LowIO     Result
	Bandwidth Result
	fileSize  int64
	filename  string
	file      *os.File
	block     []byte
}

type Result struct {
	Start             time.Time
	BlockSize         int
	TotalBytesRead    int
	TotalBytesWritten int
	IOCount           int
	Duration          time.Duration
}

func (r Result) String() string {
	return fmt.Sprintf(
		"Benchmark duration: %0.0f seconds; Block size: %d bytes; Total read: %d bytes; Total written: %d bytes; IOPS: %0.f\n",
		r.Duration.Seconds(), r.BlockSize, r.TotalBytesRead, r.TotalBytesWritten, countIOPS(r.IOCount, r.Duration),
	)
}

func (t *Runner) Init() error {
	cfg, err := t.collectFlags()
	if err != nil {
		return err
	}

	filename := path.Join(cfg.Path, "gio.dump")

	f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0600)
	if err != nil {
		return err
	}

	t.Start = time.Now()
	t.Config = cfg
	t.fileSize = int64(cfg.DiskAllocation*(1<<10)) << 20
	t.filename = filename
	t.file = f
	t.block = make([]byte, CreateFileBlockSize)

	return nil
}

func NewRunner() *Runner {
	return &Runner{
		util:      &util{},
		LowIO:     Result{},
		Bandwidth: Result{},
	}
}

func NewResult(blockSize int) *Result {
	return &Result{Start: time.Now(), BlockSize: blockSize}
}

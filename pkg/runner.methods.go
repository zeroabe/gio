package pkg

import (
	"context"
	"fmt"
	"io"
	"math/rand"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
)

// runIOPSTest performs IO benchmark
func (t *Runner) runIOPSTest(ctx context.Context, blockSize int, result *Result) error {
	readChan := make(chan int)
	writeChan := make(chan int)
	stopChan := make(chan struct{})
	errChan := make(chan error)

	defer func() {
		result.Duration = time.Now().Sub(result.Start)
	}()

	for i := 0; i < t.Config.Threads; i++ {
		go t.oneThreadIOPSTest(ctx, blockSize, readChan, writeChan, errChan, stopChan)
	}

	result.IOCount = 0

	for {
		select {
		case n := <-readChan:
			result.TotalBytesRead += n
			result.IOCount++
		case n := <-writeChan:
			result.TotalBytesWritten += n
			result.IOCount++
		case <-stopChan:
			return nil
		case err := <-errChan:
			return err
		}
	}
}

// generateBlock generates block with specified size which will be used to perform disk write operations
// Generated block doesn't contain zero values
func (t *Runner) generateBlock(blockSize int) {
	t.block = make([]byte, blockSize)
	for i := 0; i < blockSize; i++ {
		t.block[i] = genNonZeroVal()
	}
}

func genNonZeroVal() byte {
	rand.Seed(time.Now().Unix())

	for {
		i := rand.Int63()
		if i > 0 {
			return byte(i)
		}
	}
}

func (t *Runner) selfCheck() error {
	d := t.Config.Device

	isLvm, err := t.util.IsLVM(d)
	if err != nil {
		return err
	}

	if isLvm {
		return fmt.Errorf("can't execute benchmark on LVM volume")
	}

	isEmpty, err := t.util.IsEmpty(d)
	if err != nil {
		return err
	}

	if !isEmpty {
		return fmt.Errorf("can't execute benchmark on non empty volume")
	}

	return nil
}

func (t *Runner) cleanUp() {
	if err := t.file.Close(); err != nil {
		log.Errorf("can't close benchmark dump file: %s", err)
	}

	if err := os.RemoveAll(t.Config.Path); err != nil {
		log.Errorf("can't remove benchmark dump file: %s", err)
	}
}

func (t *Runner) oneThreadIOPSTest(ctx context.Context, size int, rch, wch chan<- int, errChan chan<- error, stop chan<- struct{}) {
	defer func() {
		stop <- struct{}{}
	}()

	if err := t.performIO(ctx, size, rch, wch); err != nil {
		errChan <- err
		return
	}
}

func (t *Runner) performIO(ctx context.Context, blockSize int, rch, wch chan<- int) error {
	var i int

	for {
		fileInfo, err := os.Stat(t.file.Name())
		if err != nil {
			return err
		}

		var seekOffset, rndB int64

		if fsize := fileInfo.Size(); fsize > 0 && fsize > int64(blockSize) {
			seekOffset = fsize - int64(blockSize)
		}

		if seekOffset > 0 {
			rndB = rand.Int63n(seekOffset)
		}

		_, err = t.file.Seek(rndB, 0)
		if err != nil {
			return err
		}

		select {
		case <-ctx.Done():
			return nil
		default:
			if i%10 == 0 {
				n, err := performI(t.file, t.block)
				if err != nil {
					return err
				}
				wch <- n
			} else {
				readData := make([]byte, blockSize)
				n, err := performO(t.file, readData)
				if err != nil {
					return err
				}

				rch <- n
			}
			i++
		}
	}
}

func performO(f *os.File, readData []byte) (int, error) {
	length, err := f.Read(readData)
	if err != nil {
		if err == io.EOF {
			return length, nil
		}

		return 0, err
	}

	return length, nil
}

func performI(f *os.File, writeData []byte) (int, error) {
	length, err := f.Write(writeData)
	if err != nil {
		if err == io.EOF {
			return 0, nil
		}

		return 0, err
	}

	if length != len(writeData) {
		return 0, fmt.Errorf("expected to write %d bytes instead of %d", len(writeData), length)
	}

	if err := f.Sync(); err != nil {
		return 0, err
	}

	return length, nil
}

// countIOPS returns IOPS amount
func countIOPS(operations int, duration time.Duration) float64 {
	return float64(operations) / duration.Seconds()
}

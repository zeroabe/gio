package pkg

import (
	"flag"
	"fmt"
	"io/ioutil"
	"math"
	"runtime"
	"time"
)

const (
	TestDurationDefaultValue         = 3 * time.Minute
	RushModeTestDurationDefaultValue = 30 * time.Minute
)

func (t *Runner) collectFlags() (*Config, error) {
	var device string
	cfg := &Config{}

	defaultFileSize := math.Floor(float64(2*int(t.util.GetPhysicalMemorySize()>>20))) / 1024

	flag.BoolVar(&cfg.RushMode, "rush", false, "If it set - gio will try to perform only bandwidth test fo 30 minutes")
	flag.DurationVar(&cfg.TestDuration, "t", TestDurationDefaultValue,
		"Time to run IO benchmark")
	flag.IntVar(&cfg.Threads, "threads", runtime.NumCPU(),
		"The number of concurrent processes, default is a number of CPUs")
	flag.Float64Var(&cfg.DiskAllocation, "size", defaultFileSize,
		"Amount of disk space to use (in GiB)")
	flag.StringVar(&device, "d", "",
		"Device to test (required)")
	flag.StringVar(&cfg.Path, "p", "", "path to create benchmark dump")
	flag.Parse()

	if device == "" {
		return nil, fmt.Errorf("-d flag is mandatory")
	}

	cfg.Device = device

	if cfg.Path == "" {
		defaultDir, err := t.getDefaultDir(cfg.Device)
		if err != nil {
			return nil, err
		}

		cfg.Path = defaultDir
	}

	if cfg.RushMode {
		cfg.TestDuration = RushModeTestDurationDefaultValue
	}

	return cfg, nil
}

func (t *Runner) getDefaultDir(device string) (string, error) {
	mp, err := t.util.GetDiskMountPoint(device)
	if err != nil {
		return "", err
	}

	defaultDir, err := ioutil.TempDir(mp, "gio")
	if err != nil {
		return "", err
	}

	return defaultDir, nil
}

package pkg

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"os"

	"golang.org/x/sys/unix"
)

const (
	MinBlockSize  = 1 << 9  // 512
	MaxBlockSize  = 1 << 12 // 4096 (4 KiB)
	gptHeaderSize = 92
	guidOffset    = 56
	mbrHeaderSize = MinBlockSize

	mbrPartitionTypeOffset      = 4
	mbrLVMPartitionByte    byte = 0x8E
	mprEmptyPartitionByte  byte = 0
)

var (
	lvmGUID = [16]byte{0x79, 0xd3, 0xd6, 0xe6, 0x7, 0xf5, 0xc2, 0x44, 0xa2, 0x3c, 0x23, 0x8f, 0x2a, 0x3d, 0xf9, 0x28} // E6D6D379-F507-44C2-A23C-238F2A3DF928
)

type Utility interface {
	IsLVM(device string) (bool, error)
	IsEmpty(name string) (bool, error)
	GetDiskBlockSize(name string) (int, error)
	GetPhysicalMemorySize() uint64
	GetDiskMountPoint(device string) (string, error)
}

type util struct{}

func (u *util) IsLVM(device string) (bool, error) {
	f, err := os.Open(device)
	if err != nil {
		return false, err
	}

	defer func() {
		if err := f.Close(); err != nil {
			fmt.Printf("\n[WARNING] can't close disk pipe!\n")
		}
	}()

	// Trying to read GPT table
	gptb := make([]byte, gptHeaderSize, gptHeaderSize)
	err = binary.Read(f, binary.LittleEndian, gptb)
	if err != nil {
		return false, err
	}

	// Trying to read MBR record
	mbrb := make([]byte, mbrHeaderSize, mbrHeaderSize)
	_, err = f.Seek(0, 0)
	if err != nil {
		return false, err
	}

	if _, err := f.Read(mbrb); err != nil {
		return false, err
	}

	if mbrb[mbrSignOffset] == 0x55 || mbrb[mbrSignOffset+1] == 0xAA {
		return mbrb[mbrPartitionTypeOffset] == mbrLVMPartitionByte, nil
	}

	l := guidOffset + len(lvmGUID) - 1

	return bytes.Compare(gptb[guidOffset:l], lvmGUID[:]) == 0, nil
}

func (u *util) IsEmpty(name string) (bool, error) {
	fs := unix.Statfs_t{}
	err := unix.Statfs(name, &fs)
	if err != nil {
		return false, err
	}

	return fs.Bfree == fs.Bavail && fs.Bfree > 0, nil
}

func (u *util) GetDiskBlockSize(name string) (int, error) {
	fs := unix.Statfs_t{}
	if err := unix.Statfs(name, &fs); err != nil {
		return 0, err
	}

	return int(fs.Bsize), nil
}

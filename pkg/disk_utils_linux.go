package pkg

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

const (
	devicesPath   = "/proc/devices"
	deviceTypeSD  = "sd"
	deviceTypeIDE = "ide"
)

// TODO: WIP
func listPartitions() {}

func ListDeviceTypes() ([]DeviceType, error) {
	df, err := os.Open(devicesPath)
	if err != nil {
		return nil, err
	}

	dt := make([]DeviceType, 0)

	reader := bufio.NewReader(df)
	for {
		l, isPrefix, err := reader.ReadLine()
		if err != nil {
			if err == io.EOF {
				return dt, nil
			}

			return nil, err
		}

		if isPrefix {
			continue
		}

		values := strings.Split(string(l), " ")
		if len(values) < 2 {
			continue
		}

		switch values[1] {
		case deviceTypeSD, deviceTypeIDE:
			code, err := strconv.Atoi(values[0])
			if err != nil {
				fmt.Printf("can't convert value %s to integer: %s", values[0], err)
				continue
			}

			device := DeviceType{
				Code: byte(code),
				Name: values[1],
			}

			dt = append(dt, device)
		default:
			continue
		}
	}
}

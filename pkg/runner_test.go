package pkg

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

const (
	nonZeroMemoryAmountValue uint64 = 1 << 20
)

type RunnerSuite struct {
	suite.Suite
	runner *Runner
}

var (
	uMock = &UtilsMock{}
)

func (s *RunnerSuite) SetupTest() {
	uMock.On("GetPhysicalMemorySize").Return(nonZeroMemoryAmountValue)
	uMock.On("IsLVM", "/dev/null").Return(true, nil)
	uMock.On("IsLVM", "/dev/test").Return(false, nil)
	uMock.On("IsEmpty", "/dev/test").Return(false, nil)

	s.runner = &Runner{util: uMock, Config: &Config{Device: "/dev/null"}}
}

func (s *RunnerSuite) Test_Execute_LVMFail() {
	s.runner.Config.Device = "/dev/null"
	err := s.runner.Execute()

	s.Error(err)
}

func (s *RunnerSuite) Test_Execute_IsEmptyCheckFail() {
	s.runner.Config.Device = "/dev/test"
	err := s.runner.Execute()

	s.Error(err)
}

func TestRunner_Init(t *testing.T) {
	suite.Run(t, new(RunnerSuite))
}

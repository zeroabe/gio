package pkg

import "github.com/stretchr/testify/mock"

type UtilsMock struct {
	mock.Mock
}

func (m *UtilsMock) IsLVM(device string) (bool, error) {
	args := m.Called(device)
	return args.Get(0).(bool), nil
}
func (m *UtilsMock) IsEmpty(device string) (bool, error) {
	args := m.Called(device)
	return args.Get(0).(bool), nil
}
func (m *UtilsMock) GetDiskBlockSize(device string) (int, error) {
	args := m.Called(device)
	return args.Get(0).(int), args.Get(1).(error)
}
func (m *UtilsMock) GetPhysicalMemorySize() uint64 {
	args := m.Called()
	return args.Get(0).(uint64)
}
func (m *UtilsMock) GetDiskMountPoint(device string) (string, error) {
	args := m.Called(device)
	return args.Get(0).(string), args.Get(1).(error)
}

package main

import (
	"fmt"

	"gitlab.com/zeroabe/gio/pkg"
)

func main() {
	r := pkg.NewRunner()

	err := r.Init()
	if err != nil {
		fmt.Printf("can'r create benchmark instance: %v\n", err)
		return
	}

	// TODO: exit with actual status code
	// TODO: handle os signals
	err = r.Execute()
	if err != nil {
		fmt.Printf("can'r execute test: %v\n", err)
	}
}
